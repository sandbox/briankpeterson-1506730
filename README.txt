BEFORE YOU START
----------------
You should have an understanding of creating a managing organic groups.
Please see the organic groups README.txt or go to drupal.org/project/og
This module is designed for OG 7.x-2.x only!

DESCRIPTION
-----------
OG subgroup roles creates group roles in parent group(s) for every child group.
Permissions on the parent group can be given to members of the subgroup(s)
rather than users being members of both the parent group and subgroup. Parent
group roles can be given permissions to adminster child groups or to inherit
their permissions when acting on child groups and content.
For subgroup role permissions, it is possible to select a maximum level down
the hierarchy that a user member of a subgroup will be considered a member of
the parent group. That level is selectable on a per group basis by adding the 
"subgroup permissions sublevels" field to the parent group bundle or by setting
the default sublevel setting in the OG subgroup roles admin page (admin/config/group/subgroup-roles).
Also on the admin page, it is possible to select whether OG_AUTHENTICATED_ROLE
permissions of the parent group will be granted to subgroup members, in addition
to their subgroup's permissions. Doing so is the typical behaivior for organic
groups, and the default for this module.

GETTING STARTED
---------------
When you enable the module, all existing groups are checked and subgroup
roles automatically created. In this process, default roles and
permissions are disabled for each parent group. To begin assigning permissions,
open a parent group's permissions page. There you will see a role for
each of the group's subgroups where you can give subgroup members control over
the parent group and content as you see fit. To control how many sublevels down
the hierarchy a user is allowed to gain the subgroup permissions, you need to
set the maximum sublevels option. The global setting is available in the
OG subgroup roles admin page (admin/config/groups/subgroup-roles). If you want
to set the subgroup level on a per group basis, add the "subgroup permissions
sublevels" field to the group's content type via OG field settings
(admin/config/group/fields). Permissions can be given horizontally
across subgroups by giving a subgroup role the "inherit permissions" or
"administer subgroups" permission.

CAVEATS
-------
Default roles and permissions are overridden on parent groups, this can pose an
administrative headache on sites with many groups. The problem is mitigated
since default role permissions are copied from their current state when this is
done. Also, the "administer group" permission's ultimate control over group 
content is overriden. We don't assume that a child group, despite being group
content, should be completely controlled by the parent group. This allows group
admins to retain control over their own group but not over subgroups by default.
All permissions over subgroups must be given to parent group roles via the 
"inherit permissions" or "administer subgroups" group permissions.
