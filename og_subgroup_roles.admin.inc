<?php

/**
 * @file
 * Contains configuration page settings.
 */

/**
 * Creates the admin config page.
 */
function og_subgroup_roles_settings_form($form, &$form_state) {
  $form['og_subgroup_roles_default_sublevels'] = array(
    '#type' => 'textfield',
    '#title' => t('Default maximum sublevel to give members subgroup role permissions.'),
    '#description' => t('When looking for members of a subgroup, every child subgroup of every subgroup can be considered. This setting limits how many sublevels down a member can be considered a subgroup role member.'),
    '#default_value' => variable_get('og_subgroup_roles_default_sublevels', 255),
    '#required' => TRUE,
    '#size' => 4,
    '#maxlength' => 3,
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['og_subgroup_roles_subgroups_members'] = array(
    '#type' => 'checkbox',
    '#title' => t('Additionally apply %role role permissions to subgroup members?', array('%role' => OG_AUTHENTICATED_ROLE)),
    '#description' => t("When checked, members of subgroups are given permissions assigned to the %role role in addition to permissions applied to their subgroup's role. This is how organic groups typically applies permissions.", array('%role' => OG_AUTHENTICATED_ROLE)),
    '#default_value' => variable_get('og_subgroup_roles_subgroups_members', TRUE),
  );
  return system_settings_form($form);
}
